#import matplotlib.pyplot as plt
#plt.style.use('ggplot')

from itertools import chain

import re
import nltk
import sklearn
import scipy.stats
from sklearn.metrics import make_scorer
#from sklearn.cross_validation import cross_val_score
#from sklearn.grid_search import RandomizedSearchCV

import sklearn_crfsuite
from sklearn_crfsuite import scorers
from sklearn_crfsuite import metrics

train_sents = list(nltk.corpus.conll2002.iob_sents('esp.train'))

#                1 PARTE - Criação da tagger para Part of Speach

# Etapa do Tokenizer (Identifica as palavras) e Tagger (Classifica as palavras segundo as classes
# gramaticais da lingua portuguesa)
import pickle
import nltk
#text = "Olá mundo adorável. Tudo bem como todos que estão vivendo nele ? Eles viram ?"
#Abrir o arquivo referente ao último Tagger treinado de forma binária
tagger = pickle.load(open("Nltk-Tagger-Portuguese-master/tagger.pkl", "rb"))

#============================================================================================================


#                2 PARTE - Tratando o Corpus do Harem. Dividindo-o em sentenças e palavras.
#                          Aplicando o tagger da Parte 1 para obtermos os POS de cada palavra.
#                          Cada palavra é representada por uma tupla (word, tag, "BIO"), para servir de entrada
#                          para o SKLEARN-CRF

#Extraindo o texto simples do XML e identificando as Entidades Nomeadas do HAREM
from xml.dom import minidom

fileHarem = open('CDPrimeiroHAREMprimeiroevento.xml','r', encoding="ISO-8859-1")
xmldocHarem = minidom.parse(fileHarem)
strdoc = ""
HAREMBIO = []
numSentences = 1
HAREMBIO.insert(numSentences - 1, [])
altsCurrentSentences = []
posAltsCurrentSentences = -1
lastPosLimitAlt = -1
numSentenceTagAlt = 0
numCurrentAlternatives = 0
numPrevAlternatives = 0
lastBaseSentence = []
def visitNodeHarem(node):
    #strdoc += node
    global HAREMBIO
    global numSentences

    global numSentenceTagAlt
    global numCurrentAlternatives
    global numPrevAlternatives

    global altsCurrentSentences
    global posAltsCurrentSentences
    global lastPosLimitAlt
    global lastBaseSentence


    if node.nodeType == 1 and node.tagName == "OMITIDO":
        #Não visita esse Nó e nem os seus filhos
        return 0

    if node.nodeName != "#text" and node.tagName == "ALT":
        numPrevAlternatives *= numCurrentAlternatives
        numCurrentAlternatives = 1
        posAltsCurrentSentences += 1
        if numPrevAlternatives == 0:
            numPrevAlternatives = numCurrentAlternatives


        numSentenceTagAlt += 1
        if numSentenceTagAlt == 1:
            #É a primeira tag ALT da sentença
            lastPosLimitAlt = 0
            altsCurrentSentences = []
        else:
            #Armazena o posAltsCurrentSentences de início dessa ALT para servir como
            #divisão entre as posições referentes ao último ALT e o atual
            lastPosLimitAlt = posAltsCurrentSentences

    elif node.nodeType == 3:
        noteTxt = re.sub("\.+ *\n+", ".\n<P>\n", node.nodeValue)
        words = re.split(" +|\n+", noteTxt)
        parentALT = node.parentNode.tagName == "ALT"
        grandPaALT = node.parentNode.parentNode.nodeType != 9 and node.parentNode.parentNode.tagName == "ALT"
        isInsideAlt = parentALT or grandPaALT

        if node.parentNode.tagName == "EM":
        # É um texto dentro da TAG EM, ou seja, é uma Entidade Nomeada
            parentNode = node.parentNode
            if "CATEG" in parentNode._attrs:
                categs = re.split("\|", parentNode._attrs["CATEG"].value)
                # Considerar sempre a primeira categoria
                justCateg = categs[0]
                categ = ""
                for idx, word in enumerate(words):
                    # Para cada String de frase em cada posição do Array de frases
                    # realizar o POS-TAG e assim gerar um outro array de frases com as indicações das classes gramaticais de cada palavra
                    # nessa etapa, ignora-se as quebras de linha '\n'
                    if word != "<P>":
                        tag = tagger.tag(nltk.word_tokenize(word))
                        if (len(tag) > 0):
                            tag = tag[0][1]
                        if idx == 0:
                            categ = "B-"+justCateg
                        else:
                            categ = "I-"+justCateg
                        if (word != ""):
                            if not isInsideAlt:
                                if numSentenceTagAlt == 0:
                                    #Essa sentença não possui TAG ALT até agora
                                    HAREMBIO[numSentences - 1].append((word, tag, categ))
                                else:
                                    #Existe pelo menos 1 TAG ALT na sentença atual
                                    #Adicionar a palavra atual nas posições referentes ao último ALT
                                    for i in range(lastPosLimitAlt, posAltsCurrentSentences+1):
                                        altsCurrentSentences[i].append( (word, tag, categ) )
                            elif isInsideAlt:
                                if len(altsCurrentSentences) == 0:
                                    #Copiar o parágrafo atual do HAREMBIO para o array
                                    lastBaseSentence = HAREMBIO[numSentences - 1].copy()
                                    altsCurrentSentences.insert(posAltsCurrentSentences, HAREMBIO[numSentences - 1].copy())

                                #A partir daqui nesse sentença atual usa o altsCurrentSentences
                                #A quantidade lastBaseSentsentençaence = numPrevAlternatives
                                if numPrevAlternatives == 1:
                                    #Está na primeira tag ALT da sentença
                                    if re.search("\|", word) is not None:
                                        #adiciona a parte da palavra à esqueda do | no posAltsCurrentSentences
                                        middleWordAlt = re.split("\|", word)
                                        if middleWordAlt[0] is not None and middleWordAlt[0] != "":
                                            #executa o tagger POS para essa palavra
                                            tag = tagger.tag(nltk.word_tokenize(middleWordAlt[0]))
                                            if (len(tag) > 0):
                                                tag = tag[0][1]
                                            altsCurrentSentences[posAltsCurrentSentences].append((middleWordAlt[0], tag, categ))
                                        # adiciona a parte da palavra à esqueda do | no Próximo posAltsCurrentSentences
                                        numCurrentAlternatives += 1
                                        posAltsCurrentSentences += 1
                                        # antes adiciona o LastBaseSentence na nova posição do array
                                        altsCurrentSentences.insert(posAltsCurrentSentences, lastBaseSentence)

                                        if middleWordAlt[1] is not None and middleWordAlt[1] != "":
                                            # executa o tagger POS para essa palavra
                                            tag = tagger.tag(nltk.word_tokenize(middleWordAlt[1]))
                                            if (len(tag) > 0):
                                                tag = tag[0][1]

                                            altsCurrentSentences[posAltsCurrentSentences].append((middleWordAlt[1], tag, categ))

                                    else:
                                        #adiciona a palavra na posição atual do array de alternativas
                                        altsCurrentSentences[posAltsCurrentSentences].append( (word, tag, categ) )

                                else:
                                    #Estamos no segundo ou maior ALT
                                    #Precisa adicionar a palavra atual para cada lastBaseSentence existente
                                    if len(altsCurrentSentences) <= lastPosLimitAlt:
                                        #Cópia os LastBaseSentence referentes ao ALT anterior
                                        for i in range(numPrevAlternatives):
                                            #Inicializa os LastBaseSentence
                                            altsCurrentSentences.insert(posAltsCurrentSentences, altsCurrentSentences[lastPosLimitAlt - (i+1)].copy())
                                            #adiciona a palavra atual
                                            altsCurrentSentences[posAltsCurrentSentences].append((word, tag, categ))
                                            posAltsCurrentSentences += 1
                                    else:
                                        #retorna ao pos de início; lastPosLimitAlt é maior que 0
                                        posAltsCurrentSentences = lastPosLimitAlt + (numPrevAlternatives*(numCurrentAlternatives-1))
                                        if re.search("\|", word) is not None:
                                            # adiciona a parte da palavra à esqueda do |
                                            middleWordAlt = re.split("\|", word)
                                            if middleWordAlt[0] is not None and middleWordAlt[0] != "":
                                                # executa o tagger POS para essa palavra
                                                tag = tagger.tag(nltk.word_tokenize(middleWordAlt[0]))
                                                if (len(tag) > 0):
                                                    tag = tag[0][1]
                                                for i in range(numPrevAlternatives):
                                                    # adiciona a palavra em todos os lastBaseSentence
                                                    altsCurrentSentences[posAltsCurrentSentences].append(
                                                        (middleWordAlt[0], tag, categ))
                                                    posAltsCurrentSentences += 1

                                            # adiciona a parte da palavra à esqueda do | no Próximo posAltsCurrentSentences
                                            numCurrentAlternatives += 1
                                            posAltsCurrentSentences = lastPosLimitAlt + (numPrevAlternatives*(numCurrentAlternatives-1))
                                            # antes adiciona o LastBaseSentence na nova posição do array
                                            # Cópia os LastBaseSentence referentes ao ALT anterior
                                            for i in range(numPrevAlternatives):
                                                altsCurrentSentences.insert(posAltsCurrentSentences,
                                                                            altsCurrentSentences[
                                                                                lastPosLimitAlt - (i + 1)].copy())
                                                posAltsCurrentSentences += 1
                                            #Quando sair do for, retoma ao posAltsCurrentSentences antes do for
                                            posAltsCurrentSentences = lastPosLimitAlt + (numPrevAlternatives*(numCurrentAlternatives-1))
                                            if middleWordAlt[1] is not None and middleWordAlt[1] != "":
                                                # executa o tagger POS para essa palavra
                                                tag = tagger.tag(nltk.word_tokenize(middleWordAlt[1]))
                                                if (len(tag) > 0):
                                                    tag = tag[0][1]
                                                # Adiciona o LastBaseSentence na nova posição do array
                                                for i in range(numPrevAlternatives):
                                                    # adiciona a palavra atual
                                                    altsCurrentSentences[posAltsCurrentSentences].append(
                                                        (middleWordAlt[1], tag, categ))
                                                    posAltsCurrentSentences += 1
                                        else:
                                            # adiciona a palavra à última alternativa do array
                                            posAltsCurrentSentences = lastPosLimitAlt + (numPrevAlternatives*(numCurrentAlternatives-1))
                                            for i in range(numPrevAlternatives):
                                                # adiciona a palavra
                                                altsCurrentSentences[posAltsCurrentSentences].append(
                                                    (word, tag, categ))
                                                posAltsCurrentSentences += 1

                                    #decrementar 1 pois o incremento é feito no final dos For
                                    posAltsCurrentSentences -= 1


                    else:
                        # Novo parágrafo <==> Nova sentença
                        if numSentenceTagAlt > 0:
                            for i in range(lastPosLimitAlt, posAltsCurrentSentences + 1):
                                HAREMBIO.insert(numSentences - 1, altsCurrentSentences[i].copy())
                                numSentences = numSentences + 1
                                HAREMBIO.insert(numSentences - 1, [])
                        else:
                            # se for = 0, então não existiu replicações de sentenças
                            numSentences = numSentences + 1
                            HAREMBIO.insert(numSentences - 1, [])

                        # Reinicializa as variáveis referentes as alternativas
                        altsCurrentSentences = []
                        posAltsCurrentSentences = -1
                        lastPosLimitAlt = -1
                        numSentenceTagAlt = 0
                        numCurrentAlternatives = 0
                        numPrevAlternatives = 0
                        lastBaseSentence = []
        else:
        # É um texto Fora da TAG EM, ou seja, NÃO é uma entidade nomeada
            for idx, word in enumerate(words):
                # Para cada String de frase em cada posição do Array de frases
                # realizar o POS-TAG e assim gerar um outro array de frases com as indicações das classes gramaticais de cada palavra
                # nessa etapa, ignora-se as quebras de linha '\n'
                if word != "<P>":
                    tag = tagger.tag(nltk.word_tokenize(word))
                    if (len(tag) > 0):
                        tag = tag[0][1]
                    if (word != ""):
                        if not isInsideAlt:
                            if numSentenceTagAlt == 0:
                                #Essa sentença não possui TAG ALT até agora
                                HAREMBIO[numSentences - 1].append((word, tag, "O"))
                            else:
                                #Existe pelo menos 1 TAG ALT na sentença atual
                                #Adicionar a palavra atual nas posições referentes ao último ALT
                                for i in range(lastPosLimitAlt, posAltsCurrentSentences+1):
                                    altsCurrentSentences[i].append( (word, tag, "O") )
                            #Se word = '. ' e já existir mais de 1 Alternativa. Parte a partir daqui um novo parágrafo
                            if numSentenceTagAlt > 1 and re.search(".*\.", word) is not None:
                                # Novo parágrafo <==> Nova sentença
                                if numSentenceTagAlt > 0:
                                    for i in range(lastPosLimitAlt, posAltsCurrentSentences + 1):
                                        HAREMBIO.insert(numSentences - 1, altsCurrentSentences[i].copy())
                                        numSentences = numSentences + 1
                                        HAREMBIO.insert(numSentences - 1, [])
                                else:
                                    # se for = 0, então não existiu replicações de sentenças
                                    numSentences = numSentences + 1
                                    HAREMBIO.insert(numSentences - 1, [])

                                # Reinicializa as variáveis referentes as alternativas
                                altsCurrentSentences = []
                                posAltsCurrentSentences = -1
                                lastPosLimitAlt = -1
                                numSentenceTagAlt = 0
                                numCurrentAlternatives = 0
                                numPrevAlternatives = 0
                                lastBaseSentence = []

                        elif isInsideAlt:
                            if len(altsCurrentSentences) == 0:
                                #Copiar o parágrafo atual do HAREMBIO para o array
                                lastBaseSentence = HAREMBIO[numSentences - 1].copy()
                                altsCurrentSentences.insert(posAltsCurrentSentences, HAREMBIO[numSentences - 1].copy())

                            #A partir daqui nesse sentença atual usa o altsCurrentSentences
                            #A quantidade lastBaseSentsentençaence = numPrevAlternatives
                            if numPrevAlternatives == 1:
                                #Está na primeira tag ALT da sentença
                                if re.search("\|", word) is not None:
                                    #adiciona a parte da palavra à esqueda do | no posAltsCurrentSentences
                                    middleWordAlt = re.split("\|", word)
                                    if middleWordAlt[0] is not None and middleWordAlt[0] != "":
                                        #executa o tagger POS para essa palavra
                                        tag = tagger.tag(nltk.word_tokenize(middleWordAlt[0]))
                                        if (len(tag) > 0):
                                            tag = tag[0][1]
                                        altsCurrentSentences[posAltsCurrentSentences].append((middleWordAlt[0], tag, "O"))
                                    # adiciona a parte da palavra à esqueda do | no Próximo posAltsCurrentSentences
                                    numCurrentAlternatives += 1
                                    posAltsCurrentSentences += 1
                                    # antes adiciona o LastBaseSentence na nova posição do array
                                    altsCurrentSentences.insert(posAltsCurrentSentences, lastBaseSentence)

                                    if middleWordAlt[1] is not None and middleWordAlt[1] != "":
                                        # executa o tagger POS para essa palavra
                                        tag = tagger.tag(nltk.word_tokenize(middleWordAlt[1]))
                                        if (len(tag) > 0):
                                            tag = tag[0][1]

                                        altsCurrentSentences[posAltsCurrentSentences].append((middleWordAlt[1], tag, "O"))

                                else:
                                    #adiciona a palavra na posição atual do array de alternativas
                                    altsCurrentSentences[posAltsCurrentSentences].append( (word, tag, "O") )

                            else:
                                #Estamos no segundo ou maior ALT
                                #Precisa adicionar a palavra atual para cada lastBaseSentence existente
                                if len(altsCurrentSentences) <= lastPosLimitAlt:
                                    #Cópia os LastBaseSentence referentes ao ALT anterior
                                    for i in range(numPrevAlternatives):
                                        #Inicializa os LastBaseSentence
                                        altsCurrentSentences.insert(posAltsCurrentSentences, altsCurrentSentences[lastPosLimitAlt - (i+1)].copy())
                                        #adiciona a palavra atual
                                        altsCurrentSentences[posAltsCurrentSentences].append((word, tag, "O"))
                                        posAltsCurrentSentences += 1
                                else:
                                    #retorna ao pos de início; lastPosLimitAlt é maior que 0
                                    posAltsCurrentSentences = lastPosLimitAlt + (numPrevAlternatives*(numCurrentAlternatives-1))
                                    if re.search("\|", word) is not None:
                                        # adiciona a parte da palavra à esqueda do |
                                        middleWordAlt = re.split("\|", word)
                                        if middleWordAlt[0] is not None and middleWordAlt[0] != "":
                                            # executa o tagger POS para essa palavra
                                            tag = tagger.tag(nltk.word_tokenize(middleWordAlt[0]))
                                            if (len(tag) > 0):
                                                tag = tag[0][1]
                                            for i in range(numPrevAlternatives):
                                                # adiciona a palavra em todos os lastBaseSentence
                                                altsCurrentSentences[posAltsCurrentSentences].append(
                                                    (middleWordAlt[0], tag, "O"))
                                                posAltsCurrentSentences += 1

                                        # adiciona a parte da palavra à esqueda do | no Próximo posAltsCurrentSentences
                                        numCurrentAlternatives += 1
                                        posAltsCurrentSentences = lastPosLimitAlt + (numPrevAlternatives*(numCurrentAlternatives-1))
                                        # antes adiciona o LastBaseSentence na nova posição do array
                                        # Cópia os LastBaseSentence referentes ao ALT anterior
                                        for i in range(numPrevAlternatives):
                                            altsCurrentSentences.insert(posAltsCurrentSentences,
                                                                        altsCurrentSentences[
                                                                            lastPosLimitAlt - (i + 1)].copy())
                                            posAltsCurrentSentences += 1
                                        #Quando sair do for, retoma ao posAltsCurrentSentences antes do for
                                        posAltsCurrentSentences = lastPosLimitAlt + (numPrevAlternatives*(numCurrentAlternatives-1))
                                        if middleWordAlt[1] is not None and middleWordAlt[1] != "":
                                            # executa o tagger POS para essa palavra
                                            tag = tagger.tag(nltk.word_tokenize(middleWordAlt[1]))
                                            if (len(tag) > 0):
                                                tag = tag[0][1]
                                            # Adiciona o LastBaseSentence na nova posição do array
                                            for i in range(numPrevAlternatives):
                                                # adiciona a palavra atual
                                                altsCurrentSentences[posAltsCurrentSentences].append(
                                                    (middleWordAlt[1], tag, "O"))
                                                posAltsCurrentSentences += 1
                                    else:
                                        # adiciona a palavra à última alternativa do array
                                        posAltsCurrentSentences = lastPosLimitAlt + (numPrevAlternatives*(numCurrentAlternatives-1))
                                        for i in range(numPrevAlternatives):
                                            # adiciona a palavra
                                            altsCurrentSentences[posAltsCurrentSentences].append(
                                                (word, tag, "O"))
                                            posAltsCurrentSentences += 1

                                #decrementar 1 pois o incremento é feito no final dos For
                                posAltsCurrentSentences -= 1


                else:
                    # Novo parágrafo <==> Nova sentença
                    if numSentenceTagAlt > 0:
                        for i in range(lastPosLimitAlt, posAltsCurrentSentences + 1):
                            HAREMBIO.insert(numSentences - 1, altsCurrentSentences[i].copy())
                            numSentences = numSentences + 1
                            HAREMBIO.insert(numSentences - 1, [])
                    else:
                        # se for = 0, então não existiu replicações de sentenças
                        numSentences = numSentences + 1
                        HAREMBIO.insert(numSentences - 1, [])

                    #Reinicializa as variáveis referentes as alternativas
                    altsCurrentSentences = []
                    posAltsCurrentSentences = -1
                    lastPosLimitAlt = -1
                    numSentenceTagAlt = 0
                    numCurrentAlternatives = 0
                    numPrevAlternatives = 0
                    lastBaseSentence = []
                    
    for child in node.childNodes:
        visitNodeHarem(child)

visitNodeHarem(xmldocHarem.documentElement)



#Extraindo o texto simples do XML e identificando as Entidades Nomeadas do HAREM 2
fileHarem2 = open('CDSegundoHAREM.xml','r', encoding="ISO-8859-1")
xmldocHarem2 = minidom.parse(fileHarem2)
strdoc = ""
HAREM2BIO = []
INTERVAL_IDX_DOC_HAREM2 = []
IDX_INTERVAL_DOC = -1
IDX_SENTENCE_INIT_DOC = 0
IDX_SENTENCE_FINAL_DOC = 0
currentDocID = 0
numSentences = 0
firstAlternativeInTagAlt = 1
def visitNodeHarem2(node):
    #strdoc += node
    global HAREM2BIO
    global numSentences
    global firstAlternativeInTagAlt
    global INTERVAL_IDX_DOC_HAREM2
    global IDX_INTERVAL_DOC
    global IDX_SENTENCE_INIT_DOC
    global IDX_SENTENCE_FINAL_DOC
    global currentDocID
    if node.nodeType == 1 and node.tagName == "DOC":
        if IDX_INTERVAL_DOC == -1:
            IDX_SENTENCE_INIT_DOC = numSentences
            currentDocID = node._attrs['DOCID'].value
            IDX_INTERVAL_DOC += 1
        else:
            #Armazena os idxs do DOC anterior
            IDX_SENTENCE_FINAL_DOC = numSentences - 1
            INTERVAL_IDX_DOC_HAREM2.append((IDX_SENTENCE_INIT_DOC, IDX_SENTENCE_FINAL_DOC, currentDocID))
            #Agora atualizar as variáveis referentes ao DOC atual
            IDX_INTERVAL_DOC += 1
            IDX_SENTENCE_INIT_DOC = numSentences
            currentDocID = node._attrs['DOCID'].value

    if node.nodeType == 1 and node.tagName == "OMITIDO":
        # Não visita esse Nó e nem os seus filhos
        return 0

    if node.nodeType == 1 and node.tagName == "P":
        #Novo parágrafo <==> Nova sentença
        numSentences = numSentences + 1
        HAREM2BIO.insert(numSentences-1, [])

    if node.nodeName != "#text" and node.tagName == "ALT":
        firstAlternativeInTagAlt = 1

    elif node.nodeType == 3:
        parentALT = node.parentNode.tagName == "ALT"
        grandPaALT = node.parentNode.parentNode.nodeType != 9 and node.parentNode.parentNode.tagName == "ALT"
        isInsideAlt = parentALT or grandPaALT
        if not isInsideAlt:
            firstAlternativeInTagAlt = 1

        words = re.split(" +|\n+", node.nodeValue)

        if node.parentNode.tagName == "EM":
        #É um texto dentro da TAG EM, ou seja, é um Entidade Nomeada
            if not isInsideAlt or (isInsideAlt and firstAlternativeInTagAlt):
                parentNode = node.parentNode
                if "CATEG" in parentNode._attrs:
                    categs = re.split("\|", parentNode._attrs["CATEG"].value)
                    #Considerar sempre a primeira categoria
                    categ = categs[0]
                    for idx, word in enumerate(words):
                        # Para cada String de frase em cada posição do Array de frases
                        # realizar o POS-TAG e assim gerar um outro array de frases com as indicações das classes gramaticais de cada palavra
                        # nessa etapa, ignora-se as quebras de linha '\n'

                        if isInsideAlt and re.search("\|", word) is not None:
                            #adiciona Somente a parte da palavra à esqueda do |
                            middleWordAlt = re.split("\|", word)
                            if middleWordAlt[0] is not None and middleWordAlt[0] != "":
                                #É a última palavra da primeira alternativa
                                tag = tagger.tag(nltk.word_tokenize(middleWordAlt[0]))
                                if (len(tag) > 0):
                                    tag = tag[0][1]

                                if (idx == 0):
                                    HAREM2BIO[numSentences - 1].append((middleWordAlt[0], tag, "B-" + categ))
                                else:
                                    HAREM2BIO[numSentences - 1].append((middleWordAlt[0], tag, "I-" + categ))
                            #última palavra da alternativa inserida no array
                            firstAlternativeInTagAlt = 0
                            break

                        tag = tagger.tag(nltk.word_tokenize(word))
                        if (len(tag) > 0):
                            tag = tag[0][1]

                        if(idx == 0):
                            HAREM2BIO[numSentences - 1].append( (word, tag, "B-"+categ) )
                        else:
                            HAREM2BIO[numSentences - 1].append( (word, tag, "I-"+categ) )

        else:
        # É um texto Fora da TAG EM, ou seja, NÃO é uma entidade nomeada
            if not isInsideAlt or (isInsideAlt and firstAlternativeInTagAlt):
                for idx, word in enumerate(words):
                    # Para cada String de frase em cada posição do Array de frases
                    # realizar o POS-TAG e assim gerar um outro array de frases com as indicações das classes gramaticais de cada palavra
                    # nessa etapa, ignora-se as quebras de linha '\n'

                    if isInsideAlt and re.search("\|", word) is not None:
                        # adiciona Somente a parte da palavra à esqueda do |
                        middleWordAlt = re.split("\|", word)
                        if middleWordAlt[0] is not None and middleWordAlt[0] != "":
                            # É a última palavra da primeira alternativa
                            tag = tagger.tag(nltk.word_tokenize(middleWordAlt[0]))
                            if (len(tag) > 0):
                                tag = tag[0][1]
                            if (word != ""):
                                HAREM2BIO[numSentences - 1].append((middleWordAlt[0], tag, "O"))
                        # última palavra da alternativa inserida no array
                        firstAlternativeInTagAlt = 0
                        break

                    tag = tagger.tag(nltk.word_tokenize(word))
                    if (len(tag) > 0):
                        tag = tag[0][1]
                    if(word != ""):
                        HAREM2BIO[numSentences - 1].append( (word, tag, "O") )



    for child in node.childNodes:
        visitNodeHarem2(child)

visitNodeHarem2(xmldocHarem2.documentElement)

#Armazena os idxs do DOC anterior
IDX_SENTENCE_FINAL_DOC = numSentences - 1
INTERVAL_IDX_DOC_HAREM2.append((IDX_SENTENCE_INIT_DOC, IDX_SENTENCE_FINAL_DOC, currentDocID))

#============================================================================================================


#               3 Parte: Execução do SKLEARN-CRF (precisa das palavras, as suas tags POS e tags NER) para o REN



def word2features(sent, i):
    word = sent[i][0]
    postag = sent[i][1]

    features = {
        'bias': 1.0,
        'word.lower()': word.lower(),
        'word[-3:]': word[-3:],
        'word[-2:]': word[-2:],
        'word.isupper()': word.isupper(),
        'word.istitle()': word.istitle(),
        'word.isdigit()': word.isdigit(),
        'postag': postag,
        'postag[:2]': postag[:2],
    }
    if i > 0:
        word1 = sent[i-1][0]
        postag1 = sent[i-1][1]
        features.update({
            '-1:word.lower()': word1.lower(),
            '-1:word.istitle()': word1.istitle(),
            '-1:word.isupper()': word1.isupper(),
            '-1:postag': postag1,
            '-1:postag[:2]': postag1[:2],
        })
    else:
        features['BOS'] = True

    if i < len(sent)-1:
        word1 = sent[i+1][0]
        postag1 = sent[i+1][1]
        features.update({
            '+1:word.lower()': word1.lower(),
            '+1:word.istitle()': word1.istitle(),
            '+1:word.isupper()': word1.isupper(),
            '+1:postag': postag1,
            '+1:postag[:2]': postag1[:2],
        })
    else:
        features['EOS'] = True

    return features


def sent2features(sent):
    return [word2features(sent, i) for i in range(len(sent))]

def sent2labels(sent):
    return [label for token, postag, label in sent]

def sent2tokens(sent):
    return [token for token, postag, label in sent]


'''
sent2features(train_sents[0])[0]

X_train = [sent2features(s) for s in train_sents]
y_train = [sent2labels(s) for s in train_sents]

X_test = [sent2features(s) for s in test_sents]
y_test = [sent2labels(s) for s in test_sents]
'''

#train_sents = HAREM2BIO[:1820] # Primeiro 80% do Segundo Harem para treino
#test_sents = HAREM2BIO[1821:]  # Último 20% do Segundo Harem para teste

train_sents = HAREMBIO
test_sents = HAREM2BIO

X_train = [sent2features(s) for s in train_sents]
y_train = [sent2labels(s) for s in train_sents]

X_test = [sent2features(s) for s in test_sents]
y_test = [sent2labels(s) for s in test_sents]


crf = sklearn_crfsuite.CRF(
    algorithm='lbfgs',
    c1=0.1,
    c2=0.1,
    max_iterations=100,
    all_possible_transitions=True
)
crf.fit(X_train, y_train)

# Após o treinamento, armazena o modelo treinado na raiz
with open("mycrfmodel.pkl", "wb") as f:
    pickle.dump(crf, f)

# Armazena a lista HAREMBIO com o padrão do sk-learn
with open("HAREMBIO.pkl", "wb") as f:
    pickle.dump(HAREMBIO, f)

# Armazena a lista HAREM2BIO com o padrão do sk-learn
with open("HAREM2BIO.pkl", "wb") as f:
    pickle.dump(HAREM2BIO, f)

# Armazena as informações sobre o início e fim dos documentos
with open("HAREM2BIO_INFO_DOCS.pkl", "wb") as f:
    pickle.dump(INTERVAL_IDX_DOC_HAREM2, f)

labels = list(crf.classes_)
labels.remove('O')

y_pred = crf.predict(X_test)
metrics.flat_f1_score(y_test, y_pred,
                      average='weighted', labels=labels)

sorted_labels = sorted(
    labels,
    key=lambda name: (name[1:], name[0])
)
print(metrics.flat_classification_report(
    y_test, y_pred, labels=sorted_labels, digits=3
))

#============================================================================================================


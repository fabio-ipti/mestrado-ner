import pickle
import nltk
import sklearn_crfsuite
from sklearn_crfsuite import scorers
from sklearn_crfsuite import metrics
from xml.dom import minidom
import re

# Abre o modelo crf treinado
crf = pickle.load(open("mycrfmodel.pkl", "rb"))
# Abre a lista HAREM2BIO no padrão sk-learn
HAREM2BIO = pickle.load(open("HAREM2BIO.pkl", "rb"))
test_sents = HAREM2BIO

INTERVAL_IDX_DOC_HAREM2 = pickle.load(open("HAREM2BIO_INFO_DOCS.pkl", "rb"))


def word2features(sent, i):
    word = sent[i][0]
    postag = sent[i][1]

    features = {
        'bias': 1.0,
        'word.lower()': word.lower(),
        'word[-3:]': word[-3:],
        'word[-2:]': word[-2:],
        'word.isupper()': word.isupper(),
        'word.istitle()': word.istitle(),
        'word.isdigit()': word.isdigit(),
        'postag': postag,
        'postag[:2]': postag[:2],
    }
    if i > 0:
        word1 = sent[i-1][0]
        postag1 = sent[i-1][1]
        features.update({
            '-1:word.lower()': word1.lower(),
            '-1:word.istitle()': word1.istitle(),
            '-1:word.isupper()': word1.isupper(),
            '-1:postag': postag1,
            '-1:postag[:2]': postag1[:2],
        })
    else:
        features['BOS'] = True

    if i < len(sent)-1:
        word1 = sent[i+1][0]
        postag1 = sent[i+1][1]
        features.update({
            '+1:word.lower()': word1.lower(),
            '+1:word.istitle()': word1.istitle(),
            '+1:word.isupper()': word1.isupper(),
            '+1:postag': postag1,
            '+1:postag[:2]': postag1[:2],
        })
    else:
        features['EOS'] = True

    return features


def sent2features(sent):
    return [word2features(sent, i) for i in range(len(sent))]

def sent2labels(sent):
    return [label for token, postag, label in sent]


X_test = [sent2features(s) for s in test_sents]
y_test = [sent2labels(s) for s in test_sents]


labels = list(crf.classes_)
labels.remove('O')

y_pred = crf.predict(X_test)
metrics.flat_f1_score(y_test, y_pred,
                      average='weighted', labels=labels)

sorted_labels = sorted(
    labels,
    key=lambda name: (name[1:], name[0])
)
print(metrics.flat_classification_report(
    y_test, y_pred, labels=sorted_labels, digits=3
))


#           4 Parte - Transformar o Array HAREMBIO2 para o formato do 2 HAREM com as entidades
#           e categorias definidas pelas PREDIÇÕES. Assim é possível
#           usá-lo na avaliação do SAHARA.

harem2MyEntities = []
emID = 0
idxDOC = 0
currentDOCInfo = INTERVAL_IDX_DOC_HAREM2[idxDOC]
initIdxSentence = currentDOCInfo[0]
finalIdxSentence = currentDOCInfo[1]
currentDocID = currentDOCInfo[2]

xml = """<!DOCTYPE colHAREM>
            <colHAREM versao="Segundo_dourada_11Nov2008"> """

for idxS in range(len(HAREM2BIO)):
    if idxS == initIdxSentence:
        xml+= "\n<DOC DOCID=\""+currentDocID+"\">"
    xml+="\n<P>\n"
    sentence = HAREM2BIO[idxS]
    lastWordBIO = "O"
    lastEntity = "O"
    for idxW in range(len(sentence)):
        #Obter a palavra e sua entidade classificada pelo algoritmo (o Predicted)
        word = sentence[idxW][0]
        if word == "&":
            word = "&amp;"
        sentencePred = y_pred[idxS]
        NEPredicted = y_pred[idxS][idxW]
        currentWordBIO = "O"
        entity = "O"
        if NEPredicted != "O":
            #É uma entidade Nomeada
            NEPredictedSplitted = re.split("-", NEPredicted)
            isNERBegin = NEPredictedSplitted[0] == "B"
            currentWordBIO = NEPredictedSplitted[0]
            entity = NEPredictedSplitted[1]
            if isNERBegin:
                #Inicia a TAG EM
                if lastWordBIO != "O":
                    # Fecha a Tag EM da entidade anterior
                    xml += "</EM>"
                emID += 1
                xml += """ <EM ID="{}" CATEG="{}">{}""".format(emID, entity, word)
            else:
                #Verificar se é um Falso início de entidade
                if entity != lastEntity:
                    # Inicia a TAG EM
                    if lastWordBIO != "O":
                        # Fecha a Tag EM da entidade anterior
                        xml += "</EM>"
                    emID += 1
                    xml += """ <EM ID="{}" CATEG="{}">{}""".format(emID, entity, word)
                else:
                    #A palavra faz parte da entidade corrente
                    xml += """ {}""".format(word)
            # Se for a última palavra da sentença, então fecha o EM corrente
            if idxW == len(sentence) - 1:
                # Fecha a Tag EM da entidade atual
                xml += "</EM>"
        else:
            # Não é uma entidade Nomeada
            if lastWordBIO != "O":
                # Fecha a Tag EM da entidade anterior
                xml += "</EM>"
            xml += """ {}""".format(word)

        lastWordBIO = currentWordBIO
        lastEntity = entity

    # Fecha o parágrafo
    xml += "\n</P>"
    if idxS == finalIdxSentence:
        xml+= "\n</DOC>"
        #avança para o próximo documento, se existir
        idxDOC += 1
        if idxDOC < len(INTERVAL_IDX_DOC_HAREM2):
            currentDOCInfo = INTERVAL_IDX_DOC_HAREM2[idxDOC]
            initIdxSentence = currentDOCInfo[0]
            finalIdxSentence = currentDOCInfo[1]
            currentDocID = currentDOCInfo[2]

xml += """\n </colHAREM>"""
xmldoc = minidom.parseString(xml)
xmldoc
